# Start Menu Cleaner

A TUI Python script to quickly clear the start menu of all the folders.

## Dependencies

[Colorama for Python](https://pypi.org/project/colorama/)

## Commands

Commands are stripped of spaces, and as such, are not needed.

- **q:** Quit
- **s:** Go to next folder, stop
- **a:** Move all files in folder out, continue
- **d:** Delete folder and contents, go to next folder, stop
- **a *parameter\**:** Move *parameter*-th file in folder out, continue
- **d *parameter\**:** Delete *parameter*-th file in folder, continue

\**parameter*: a single or list of integers separated by commas (,)

## Chaining commands

Commands can be chained, if separated by semicolons (;), assuming they say continue. A command that end in a stop will finish the process, and any subsequent command will be ignored. An error counts as a stop.

## Example usage

- `>> a 2` : Move the second item out into the main folder, then stop
- `>> d 1, 2` : Delete the first and second item, then stop
- `>> a1;d2` : Move the first item out into the main folder, delete the second item, then stop (Notice that the index does not change)
- `>> d;s` : Delete folder, then stop, *the go to next folder command is never executed*
